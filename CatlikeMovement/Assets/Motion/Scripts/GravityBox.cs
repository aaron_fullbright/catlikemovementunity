using System;
using UnityEngine;

namespace Motion.Scripts
{
    public class GravityBox : GravitySource
    {
        [SerializeField]
        private float Gravity = 9.81f;

        [SerializeField]
        private Vector3 BoundaryDistance = Vector3.one;

        [SerializeField, Min(0f)]
        private float InnerDistance = 0f, InnerFalloffDistance = 0f;
        
        [SerializeField, Min(0f)]
        private float OuterDistance = 0f, OuterFalloffDistance = 0f;

        private float _innerFalloffFactor, _outerFalloffFactor;
        
        private void OnValidate()
        {
            BoundaryDistance = Vector3.Max(BoundaryDistance, Vector3.zero);
            var maxInner = Mathf.Min(Mathf.Min(BoundaryDistance.x, BoundaryDistance.y), BoundaryDistance.z);
            InnerDistance = Mathf.Min(InnerDistance, maxInner);
            InnerFalloffDistance = Mathf.Max(Mathf.Min(InnerFalloffDistance, maxInner), InnerDistance);

            OuterFalloffDistance = Mathf.Max(OuterFalloffDistance, OuterDistance);
            _innerFalloffFactor = 1f / (InnerFalloffDistance - InnerDistance);
            _outerFalloffFactor = 1f / (OuterFalloffDistance - OuterDistance);
        }

        private void Awake()
        {
            OnValidate();
        }

        private float GetGravityComponent(float coordinate, float distance)
        {
            if (distance > InnerFalloffDistance)
            {
                return 0f;
            }
            var g = Gravity;
            if (distance > InnerDistance)
            {
                g *= 1f - (distance - InnerDistance) * _innerFalloffFactor;
            }
            return coordinate > 0f ? -g : g;
        }

        public override Vector3 GetGravity(Vector3 position)
        {
            position = transform.InverseTransformDirection(position - transform.position);
            var vector = Vector3.zero;

            var outside = 0;
            if (position.x > BoundaryDistance.x)
            {
                vector.x = BoundaryDistance.x - position.x;
                outside = 1;
            }
            else if (position.x < -BoundaryDistance.x)
            {
                vector.x = -BoundaryDistance.x - position.x;
                outside = 1;
            }
            
            if (position.y > BoundaryDistance.y) {
                vector.y = BoundaryDistance.y - position.y;
                outside += 1;
            }
            else if (position.y < -BoundaryDistance.y) {
                vector.y = -BoundaryDistance.y - position.y;
                outside += 1;
            }

            if (position.z > BoundaryDistance.z) {
                vector.z = BoundaryDistance.z - position.z;
                outside += 1;
            }
            else if (position.z < -BoundaryDistance.z) {
                vector.z = -BoundaryDistance.z - position.z;
                outside += 1;
            }

            if (outside > 0)
            {
                var distance = vector.magnitude;
                if (distance > OuterFalloffDistance)
                {
                    return Vector3.zero;
                }

                var g = Gravity / distance;
                if (distance > OuterDistance)
                {
                    g *= 1f - (distance - OuterDistance) * _outerFalloffFactor;
                }

                return transform.TransformDirection(g * vector);
            }
            
            Vector3 distances;
            distances.x = BoundaryDistance.x - Mathf.Abs(position.x);
            distances.y = BoundaryDistance.y - Mathf.Abs(position.y);
            distances.z = BoundaryDistance.z - Mathf.Abs(position.z);
            if (distances.x < distances.y)
            {
                if (distances.x < distances.z)
                {
                    vector.x = GetGravityComponent(position.x, distances.x);
                }
                else
                {
                    vector.z = GetGravityComponent(position.z, distances.z);
                }
            }
            else if (distances.y < distances.z)
            {
                vector.y = GetGravityComponent(position.y, distances.y);
            }
            else
            {
                vector.z = GetGravityComponent(position.z, distances.z);
            }

            return transform.TransformDirection(vector);
        }

        private void OnDrawGizmos()
        {
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Vector3 size;
            if (InnerFalloffDistance > InnerDistance)
            {
                Gizmos.color = Color.cyan;
                size.x = 2f * (BoundaryDistance.x - InnerFalloffDistance);
                size.y = 2f * (BoundaryDistance.y - InnerFalloffDistance);
                size.z = 2f * (BoundaryDistance.z - InnerFalloffDistance);
                Gizmos.DrawWireCube(Vector3.zero, size);
            }

            if (InnerDistance > 0f)
            {
                Gizmos.color = Color.yellow;
                size.x = 2f * (BoundaryDistance.x - InnerDistance);
                size.y = 2f * (BoundaryDistance.y - InnerDistance);
                size.z = 2f * (BoundaryDistance.z - InnerDistance);
                Gizmos.DrawWireCube(Vector3.zero, size);
            }
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Vector3.zero, 2f * BoundaryDistance);

            if (OuterDistance > 0f)
            {
                Gizmos.color = Color.yellow;
                DrawGizmosOuterCube(OuterDistance);
            }

            if (OuterFalloffDistance > OuterDistance)
            {
                Gizmos.color = Color.cyan;
                DrawGizmosOuterCube(OuterFalloffDistance);
            }
        }

        private void DrawGizmosRect(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            Gizmos.DrawLine(a, b);
            Gizmos.DrawLine(b, c);
            Gizmos.DrawLine(c, d);
            Gizmos.DrawLine(d, a);
        }

        private void DrawGizmosOuterCube(float distance)
        {
            Vector3 a, b, c, d;
            a.y = b.y = BoundaryDistance.y;
            d.y = c.y = -BoundaryDistance.y;
            b.z = c.z = BoundaryDistance.z;
            d.z = a.z = -BoundaryDistance.z;
            a.x = b.x = c.x = d.x = BoundaryDistance.x + distance;
            DrawGizmosRect(a, b, c, d);
            a.x = b.x = c.x = d.x = -a.x;
            DrawGizmosRect(a, b, c, d);
            
            a.x = d.x = BoundaryDistance.x;
            b.x = c.x = -BoundaryDistance.x;
            a.z = b.z = BoundaryDistance.z;
            c.z = d.z = -BoundaryDistance.z;
            a.y = b.y = c.y = d.y = BoundaryDistance.y + distance;
            DrawGizmosRect(a, b, c, d);
            a.y = b.y = c.y = d.y = -a.y;
            DrawGizmosRect(a, b, c, d);

            a.x = d.x = BoundaryDistance.x;
            b.x = c.x = -BoundaryDistance.x;
            a.y = b.y = BoundaryDistance.y;
            c.y = d.y = -BoundaryDistance.y;
            a.z = b.z = c.z = d.z = BoundaryDistance.z + distance;
            DrawGizmosRect(a, b, c, d);
            a.z = b.z = c.z = d.z = -a.z;
            DrawGizmosRect(a, b, c, d);
            
            distance *= 0.5773502692f;
            var size = BoundaryDistance;
            size.x = 2f * (size.x + distance);
            size.y = 2f * (size.y + distance);
            size.z = 2f * (size.z + distance);
            Gizmos.DrawWireCube(Vector3.zero, size);
        }
    }
}