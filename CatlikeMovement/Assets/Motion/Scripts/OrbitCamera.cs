using System.Collections;
using Rewired;
using RewiredConsts;
using UnityEngine;
using Player = Rewired.Player;

namespace Motion.Scripts
{
    [RequireComponent(typeof(Camera))]
    public class OrbitCamera : MonoBehaviour
    {
        [SerializeField]
        private Transform Focus = default;

        [SerializeField, Range(1f, 20f)]
        private float Distance = 5f;

        [SerializeField, Min(0f)]
        private float FocusRadius = 1f;

        [SerializeField, Range(0f, 1f)]
        private float FocusCentering = 0.75f;

        [SerializeField, Range(1f, 360f)]
        private float _rotationSpeed = 90f;

        [SerializeField, Range(-89f, 89f)]
        private float MinVerticalAngle = 30f, MaxVerticalAngle = 60f;

        [SerializeField, Min(0f)]
        private float AlignDelay = 5f;

        [SerializeField, Min(0f)]
        private float UpAlignmentSpeed = 360f;

        private float _lastManualRotationTime;

        private Vector3 _focusPoint, _previousFocusPoint;
        private Vector2 _orbitAngles = new Vector2(45f, 0f);

        private Player _input;
        private Camera _camera;

        [SerializeField]
        private LayerMask ObstructionMask = -1;

        private Quaternion _gravityAlignment = Quaternion.identity;
        private Quaternion _orbitRotation;
        
        private Vector3 CameraHalfExtents
        {
            get
            {
                Vector3 halfExtends;
                halfExtends.y = _camera.nearClipPlane * Mathf.Tan(0.5f * Mathf.Deg2Rad * _camera.fieldOfView);
                halfExtends.x = halfExtends.y * _camera.aspect;
                halfExtends.z = 0f;
                return halfExtends;
            }
        }

        private void OnValidate()
        {
            if (MaxVerticalAngle < MinVerticalAngle)
                MaxVerticalAngle = MinVerticalAngle;
        }

        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _focusPoint = Focus.position;
            transform.localRotation = _orbitRotation = Quaternion.Euler(_orbitAngles);
        }

        private IEnumerator Start()
        {
            if (!ReInput.isReady)
                yield return null;
            _input = ReInput.players.GetPlayer(0);
        }

        private void LateUpdate()
        {
            UpdateGravityAlignment();
            UpdateFocusPoint();
            if (ManualRotation() || AutomaticRotation())
            {
                ConstrainAngles();
                _orbitRotation = Quaternion.Euler(_orbitAngles);
            }

            var lookRotation = _gravityAlignment * _orbitRotation;
            
            var lookDirection = transform.forward;
            var lookPosition = _focusPoint - lookDirection * Distance;
            var rectOffset = lookDirection * _camera.nearClipPlane;
            var rectPosition = lookPosition + rectOffset;
            var castFrom = Focus.position;
            var castLine = rectPosition - castFrom;
            var castDistance = castLine.magnitude;
            var castDirection = castLine / castDistance;

            if (Physics.BoxCast(castFrom, CameraHalfExtents, castDirection, out var hit, lookRotation, castDistance, ObstructionMask))
            {
                rectPosition = castFrom + castDirection * hit.distance;
                lookPosition = rectPosition - rectOffset;
            }
            
            transform.SetPositionAndRotation(lookPosition, lookRotation);
        }

        private void UpdateGravityAlignment()
        {
            var fromUp = _gravityAlignment * Vector3.up;
            var toUp = CustomGravity.GetUpAxis(_focusPoint);
            var dot = Mathf.Clamp(Vector3.Dot(fromUp, toUp), -1f, 1f);
            var angle = Mathf.Acos(dot) * Mathf.Rad2Deg;
            var maxAngle = UpAlignmentSpeed * Time.deltaTime;

            var newAlignment = Quaternion.FromToRotation(fromUp, toUp) * _gravityAlignment;
            _gravityAlignment = angle <= maxAngle ? newAlignment : Quaternion.SlerpUnclamped(_gravityAlignment, newAlignment, maxAngle / angle);
        }

        private void UpdateFocusPoint()
        {
            _previousFocusPoint = _focusPoint;
            var targetPoint = Focus.position;
            if (FocusRadius > 0f)
            {
                var distance = Vector3.Distance(targetPoint, _focusPoint);
                if (distance > FocusRadius)
                {
                    _focusPoint = Vector3.Lerp(targetPoint, _focusPoint, FocusRadius / distance);
                }

                if (distance > 0.01f && FocusCentering > 0f)
                {
                    _focusPoint = Vector3.Lerp(targetPoint, _focusPoint, Mathf.Pow(1f - FocusCentering, Time.unscaledDeltaTime));
                }
            }
            else
            {
                _focusPoint = targetPoint;
            }
        }

        private bool ManualRotation()
        {
            var input = _input.GetAxis2D(Action.CameraVertical, Action.CameraHorizontal);

            const float e = 0.001f;
            if (input.x < -e || input.x > e || input.y < -e || input.y > e)
            {
                _orbitAngles += _rotationSpeed * Time.unscaledDeltaTime * input;
                _lastManualRotationTime = Time.unscaledTime;
                return true;
            }

            return false;
        }

        private void ConstrainAngles()
        {
            _orbitAngles.x = Mathf.Clamp(_orbitAngles.x, MinVerticalAngle, MaxVerticalAngle);
            if (_orbitAngles.y < 0f)
                _orbitAngles.y += 360f;
            else if (_orbitAngles.y >= 360f)
                _orbitAngles.y -= 360f;
        }

        [SerializeField, Range(0, 90f)]
        private float AlignSmoothRange = 45f;

        private bool AutomaticRotation()
        {
            if (Time.unscaledTime - _lastManualRotationTime < AlignDelay)
                return false;
            var alignedDelta = Quaternion.Inverse(_gravityAlignment) * (_focusPoint - _previousFocusPoint);
            var movement = new Vector2(alignedDelta.x, alignedDelta.z);
            var movementDeltaSqr = movement.sqrMagnitude;
            if (movementDeltaSqr < 0.000001f)
                return false;

            var headingAngle = GetAngle(movement / Mathf.Sqrt(movementDeltaSqr));
            var deltaAbs = Mathf.Abs(Mathf.DeltaAngle(_orbitAngles.y, headingAngle));
            var rotationChange = _rotationSpeed * Mathf.Min(Time.unscaledDeltaTime, movementDeltaSqr);
            if (deltaAbs < AlignSmoothRange)
            {
                rotationChange *= deltaAbs / AlignSmoothRange;
            }
            else if (180f - deltaAbs < AlignSmoothRange)
            {
                rotationChange *= (180f - deltaAbs) / AlignSmoothRange;
            }
            _orbitAngles.y = Mathf.MoveTowardsAngle(_orbitAngles.y, headingAngle, rotationChange);
            return true;
        }

        private static float GetAngle(Vector2 direction)
        {
            var angle = Mathf.Acos(direction.y) * Mathf.Rad2Deg;
            return direction.x < 0f ? 360f - angle : angle;
        }
    }
}