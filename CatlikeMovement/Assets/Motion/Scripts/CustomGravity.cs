using System.Collections.Generic;
using UnityEngine;

namespace Motion.Scripts
{
    public static class CustomGravity
    {
        private static List<GravitySource> _sources = new List<GravitySource>();

        public static Vector3 GetGravity(Vector3 position)
        {
            var g = Vector3.zero;
            foreach (var source in _sources)
            {
                g += source.GetGravity(position);
            }

            return g;
        }

        public static Vector3 GetUpAxis(Vector3 position)
        {
            var g = Vector3.zero;
            foreach (var source in _sources)
            {
                g += source.GetGravity(position);
            }
            return -g.normalized;
        }

        public static Vector3 GetGravity(Vector3 position, out Vector3 upAxis)
        {
            var g = Vector3.zero;
            foreach (var source in _sources)
            {
                g += source.GetGravity(position);
            }

            upAxis = -g.normalized;
            return g;
        }

        public static void Register(GravitySource source)
        {
            Debug.Assert(!_sources.Contains(source), "Duplicate registration of gravity source!", source);
            _sources.Add(source);
        }

        public static void Unregister(GravitySource source)
        {
            Debug.Assert(!_sources.Contains(source), "Unregistering unknown gravity source!", source);
            _sources.Remove(source);
        }
    }
}