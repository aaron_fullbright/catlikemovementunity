using UnityEngine;

namespace Motion.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class CustomGravityRigidbody : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private float _floatDelay;

        [SerializeField]
        private bool FloatToSleep;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.useGravity = false;
        }

        private void FixedUpdate()
        {
            if (FloatToSleep)
            {
                if (_rigidbody.IsSleeping())
                {
                    _floatDelay = 0f;
                    return;
                }

                if (_rigidbody.velocity.sqrMagnitude < 0.0001f)
                {
                    _floatDelay += Time.deltaTime;
                    if (_floatDelay >= 1f)
                    {
                        return;
                    }
                }
                else
                {
                    _floatDelay = 0f;
                }  
            }

            _rigidbody.AddForce(CustomGravity.GetGravity(_rigidbody.position), ForceMode.Acceleration);
        }
    }
}