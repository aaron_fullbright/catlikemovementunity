using UnityEngine;

namespace Motion.Scripts
{
    public class GravitySphere : GravitySource
    {
        [SerializeField]
        private float Gravity = 9.81f;

        [SerializeField, Min(0f)]
        private float OuterRadius = 10f, OuterFalloffRadius = 15f;

        [SerializeField, Min(0f)]
        private float InnerRadius = 1f, InnerFalloffRadius = 5f;

        
        private float _outerFalloffFactor, _innerFalloffFactor;
        
        private void OnValidate()
        {
            InnerFalloffRadius = Mathf.Max(InnerFalloffRadius, 0f);
            InnerRadius = Mathf.Max(InnerRadius, InnerFalloffRadius);
            OuterRadius = Mathf.Max(OuterRadius, InnerRadius);
            OuterFalloffRadius = Mathf.Max(OuterFalloffRadius, OuterRadius);
            
            _innerFalloffFactor = 1f / (InnerRadius - InnerFalloffRadius);
            _outerFalloffFactor = 1f / (OuterFalloffRadius - OuterRadius);
        }

        private void Awake()
        {
            OnValidate();
        }

        private void OnDrawGizmos()
        {
            var p = transform.position;

            if (InnerFalloffRadius > 0f && InnerFalloffRadius < InnerRadius)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(p, InnerFalloffRadius);
            }
            Gizmos.color = Color.yellow;
            if (InnerRadius > 0f && InnerRadius < OuterRadius)
            {
                Gizmos.DrawWireSphere(p, InnerRadius);
            }
            
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(p, OuterRadius);
            if (OuterFalloffRadius > OuterRadius)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(p, OuterFalloffRadius);
            }
        }

        public override Vector3 GetGravity(Vector3 position)
        {
            var vector = transform.position - position;
            var distance = vector.magnitude;
            if (distance > OuterFalloffRadius || distance < InnerFalloffRadius)
            {
                return Vector3.zero;
            }

            var g = Gravity / distance;
            if (distance > OuterRadius)
            {
                g *= 1f - (distance - OuterRadius) * _outerFalloffFactor;
            }
            else if (distance < InnerRadius)
            {
                g *= 1f - (InnerRadius - distance) * _innerFalloffFactor;
            }
            return g * vector;
        }
    }
}