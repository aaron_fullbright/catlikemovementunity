using System;
using System.Collections;
using Rewired;
using UnityEngine;
using Action = RewiredConsts.Action;
using Player = Rewired.Player;

namespace Motion.Scripts
{
    public class MovingSphere : MonoBehaviour
    {
        private Player _input;

        [SerializeField, Range(0, 100f)]
        private float MaxAccel;

        [SerializeField, Range(0, 100f)]
        private float MaxAirAccel;

        [SerializeField, Range(0, 100f)]
        private float MaxSpeed;

        [SerializeField, Range(0, 10f)]
        private float JumpHeight;

        [SerializeField, Range(0, 90f)]
        private float MaxGroundAngle = 25f, MaxStairsAngle = 50f;

        [SerializeField, Range(0, 100f)]
        private float MaxSnapSpeed = 100f;

        [SerializeField, Min(0f)]
        private float ProbeDistance;

        [SerializeField]
        private LayerMask ProbeMask = -1, StairsMask = -1;

        private float _minGroundDotProduct, _minStairsDotProduct;

        private Vector3 _velocity, _desiredVelocity, _contactNormal, _steepNormal, _upAxis, _rightAxis, _forwardAxis;
        private Rigidbody _body;

        private bool _jumpQueued;
        private int _groundContactCount, _steepContactCount;

        private bool OnGround => _groundContactCount > 0;
        private bool OnSteep => _steepContactCount > 0;

        [SerializeField, Range(0, 5)]
        private int MaxAirJumps;

        private int _jumpPhase;

        private Renderer _renderer;
        private static readonly int BaseColor = Shader.PropertyToID("_BaseColor");

        [SerializeField]
        private bool UseJumpBias;

        [SerializeField]
        private Transform PlayerInputSpace;


        public enum ColorSetting
        {
            None,
            ContactCount,
            Airborne
        }

        public ColorSetting ColorMode;

        private int _stepsSinceLastGrounded, _stepsSinceLastJump;


        private void OnValidate()
        {
            _minGroundDotProduct = Mathf.Cos(MaxGroundAngle * Mathf.Deg2Rad);
            _minStairsDotProduct = Mathf.Cos(MaxStairsAngle * Mathf.Deg2Rad);
        }

        private void Awake()
        {
            _body = GetComponent<Rigidbody>();
            _renderer = GetComponent<Renderer>();
            _body.useGravity = false;
            OnValidate();
        }

        private IEnumerator Start()
        {
            if (!ReInput.isReady)
                yield return null;
            _input = ReInput.players.GetPlayer(0);
        }

        private void Update()
        {
            var inputVector = _input.GetAxis2D(Action.Horizontal, Action.Vertical);
            inputVector = Vector2.ClampMagnitude(inputVector, 1f);

            if (PlayerInputSpace)
            {
                _rightAxis = ProjectDirectionOnPlane(PlayerInputSpace.right, _upAxis);
                _forwardAxis = ProjectDirectionOnPlane(PlayerInputSpace.forward, _upAxis);
            }
            else
            {
                _rightAxis = ProjectDirectionOnPlane(Vector3.right, _upAxis);
                _forwardAxis = ProjectDirectionOnPlane(Vector3.forward, _upAxis);
            }

            _desiredVelocity = new Vector3(inputVector.x, 0f, inputVector.y) * MaxSpeed;

            _jumpQueued |= _input.GetButtonDown(Action.Jump);

            switch (ColorMode)
            {
                case ColorSetting.ContactCount:
                    _renderer.material.SetColor(BaseColor, Color.cyan * _groundContactCount * 0.25f);
                    break;
                case ColorSetting.Airborne:
                    _renderer.material.SetColor(BaseColor, OnGround ? Color.green : Color.cyan);
                    break;
                case ColorSetting.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void FixedUpdate()
        {
            var gravity = CustomGravity.GetGravity(_body.position, out _upAxis);
            UpdateState();
            AdjustVelocity();

            if (_jumpQueued)
            {
                _jumpQueued = false;
                Jump(gravity);
            }

            _velocity += gravity * Time.deltaTime;

            _body.velocity = _velocity;
            ClearState();
        }

        private void ClearState()
        {
            _groundContactCount = _steepContactCount = 0;
            _contactNormal = _steepNormal = Vector3.zero;
        }

        private void UpdateState()
        {
            _stepsSinceLastGrounded++;
            _stepsSinceLastJump++;
            _velocity = _body.velocity;
            if (OnGround || SnapToGround() || CheckSteepContacts())
            {
                _stepsSinceLastGrounded = 0;
                if (_stepsSinceLastJump > 1)
                {
                    _jumpPhase = 0;
                }

                if (_groundContactCount > 1)
                {
                    _contactNormal.Normalize();
                }
            }
            else
            {
                _contactNormal = _upAxis;
            }
        }

        private bool SnapToGround()
        {
            if (_stepsSinceLastGrounded > 1 || _stepsSinceLastJump <= 2)
                return false;
            var speed = _velocity.magnitude;
            if (speed > MaxSnapSpeed)
                return false;
            if (!Physics.Raycast(_body.position, -_upAxis, out var hit, ProbeDistance, ProbeMask))
                return false;
            var updot = Vector3.Dot(_upAxis, hit.normal);
            if (updot < GetMinDot(hit.collider.gameObject.layer))
                return false;

            _groundContactCount = 1;
            _contactNormal = hit.normal;

            var dot = Vector3.Dot(_velocity, hit.normal);
            if (dot > 0f)
                _velocity = (_velocity - hit.normal * dot).normalized * speed;

            return true;
        }

        private void OnCollisionEnter(Collision other)
        {
            EvaluateCollision(other);
        }

        private void OnCollisionStay(Collision other)
        {
            EvaluateCollision(other);
        }

        private void EvaluateCollision(Collision collision)
        {
            var minDot = GetMinDot(collision.gameObject.layer);
            for (var i = 0; i < collision.contactCount; i++)
            {
                var normal = collision.GetContact(i).normal;
                var updot = Vector3.Dot(_upAxis, normal);
                if (updot >= minDot)
                {
                    _groundContactCount++;
                    _contactNormal += normal;
                }
                else if (updot > -0.01f)
                {
                    _steepContactCount++;
                    _steepNormal += normal;
                }
            }
        }

        private bool CheckSteepContacts()
        {
            if (_steepContactCount > 1)
            {
                _steepNormal.Normalize();
                var updot = Vector3.Dot(_upAxis, _steepNormal);
                if (updot >= _minGroundDotProduct)
                {
                    _steepContactCount = 0;
                    _groundContactCount = 1;
                    _contactNormal = _steepNormal;
                    return true;
                }
            }

            return false;
        }

        private void Jump(Vector3 gravity)
        {
            Vector3 jumpDirection;

            if (OnGround)
            {
                jumpDirection = _contactNormal;
            }
            else if (OnSteep)
            {
                jumpDirection = _steepNormal;
                _jumpPhase = 0;
            }
            else if (MaxAirJumps > 0 && _jumpPhase <= MaxAirJumps)
            {
                if (_jumpPhase == 0)
                {
                    _jumpPhase = 1;
                }

                jumpDirection = _contactNormal;
            }
            else
            {
                return;
            }

            _stepsSinceLastJump = 0;
            _jumpPhase++;
            var jumpSpeed = Mathf.Sqrt(2f * gravity.magnitude * JumpHeight);
            if (UseJumpBias)
                jumpDirection = (jumpDirection + _upAxis).normalized;
            var alignedSpeed = Vector3.Dot(_velocity, jumpDirection);
            if (alignedSpeed > Mathf.Epsilon)
            {
                jumpSpeed = Mathf.Max(jumpSpeed - alignedSpeed, 0f);
            }

            _velocity += jumpDirection * jumpSpeed;
        }

        // private Vector3 ProjectOnPlane(Vector3 vector) => vector - _contactNormal * Vector3.Dot(vector, _contactNormal);

        private Vector3 ProjectDirectionOnPlane(Vector3 direction, Vector3 normal) => (direction - normal * Vector3.Dot(direction, normal)).normalized;

        private void AdjustVelocity()
        {
            var xAxis = ProjectDirectionOnPlane(_rightAxis, _contactNormal);
            var zAxis = ProjectDirectionOnPlane(_forwardAxis, _contactNormal);
            var currentX = Vector3.Dot(_velocity, xAxis);
            var currentZ = Vector3.Dot(_velocity, zAxis);
            var accel = OnGround ? MaxAccel : MaxAirAccel;
            var newX = Mathf.MoveTowards(currentX, _desiredVelocity.x, accel * Time.deltaTime);
            var newZ = Mathf.MoveTowards(currentZ, _desiredVelocity.z, accel * Time.deltaTime);
            _velocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);
        }

        private float GetMinDot(int layer) => (StairsMask & (1 << layer)) == 0 ? _minGroundDotProduct : _minStairsDotProduct;
    }
}