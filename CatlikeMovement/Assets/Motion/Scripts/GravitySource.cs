using UnityEngine;

namespace Motion.Scripts
{
    public abstract class GravitySource : MonoBehaviour
    {
        public virtual Vector3 GetGravity(Vector3 position) => Physics.gravity;

        private void OnEnable()
        {
            CustomGravity.Register(this);
        }

        private void OnDisable()
        {
            CustomGravity.Unregister(this);
        }
    }
}