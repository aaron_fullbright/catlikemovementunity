using UnityEngine;

namespace Motion.Scripts
{
    public class GravityPlane : GravitySource
    {
        [SerializeField]
        private float Gravity = 9.81f;

        [SerializeField, Min(0f)]
        private float Range = 1f;

        public override Vector3 GetGravity(Vector3 position)
        {
            var up = transform.up;
            var distance = Vector3.Dot(up, position - transform.position);
            if (distance > Range)
            {
                return Vector3.zero;
            }

            var g = -Gravity;
            if (distance > 0f)
            {
                g *= 1f - distance / Range;
            }

            return g * up;
        }

        private void OnDrawGizmos()
        {
            var scale = transform.localScale;
            scale.y = Range;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, scale);
            var size = new Vector3(1f, 0, 1f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(Vector3.zero, size);
            if (Range > 0f)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireCube(Vector3.up, size);
            }
        }
    }
}